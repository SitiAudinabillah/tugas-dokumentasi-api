-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 31, 2022 at 02:22 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `task_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `start_date` date NOT NULL DEFAULT current_timestamp(),
  `finish_date` date NOT NULL DEFAULT current_timestamp(),
  `status` enum('New','On Progress','Finish') NOT NULL,
  `doc_url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `category_id`, `title`, `description`, `start_date`, `finish_date`, `status`, `doc_url`) VALUES
(1, 1, 'task 1', 'task making indomie', '2022-10-31', '2022-10-31', 'On Progress', 'Indomie.pptx'),
(3, 2, 'tasky', 'baking class diary', '2022-10-31', '2022-10-31', 'Finish', 'tasky.pdf'),
(4, 2, 'my task', 'task building rock', '2022-10-31', '2022-10-31', 'On Progress', 'skrip rock.docx'),
(6, 3, 'Assignment', 'homework assignment', '2022-10-31', '2022-10-31', 'New', 'skript.docx'),
(7, 4, 'Design', 'New York Fashion Week', '2022-10-31', '2022-10-31', 'On Progress', 'NYFW.pptx'),
(8, 4, 'Design Studios', 'Make over room', '2022-10-31', '2022-10-31', 'On Progress', 'pic.jpg'),
(9, 5, 'Medicine', 'Medicine for coughing', '2022-10-31', '2022-10-31', 'On Progress', 'cough syrup.xml'),
(10, 6, 'Finance', 'housing finance', '2022-10-31', '2022-10-31', 'Finish', 'housing.xml'),
(11, 7, 'Sponsor', 'Sponsor for Csf', '2022-10-31', '2022-10-31', 'Finish', 'csf.pptx'),
(12, 8, 'Subsidize', 'vegetable oil', '2022-10-31', '2022-10-31', 'On Progress', 'sembako.doc'),
(13, 9, 'Rules', 'dont break', '2022-10-31', '2022-10-31', 'New', 'must.pdf'),
(15, 11, 'nurse', 'take care others', '2022-10-31', '2022-10-31', 'Finish', 'note.pdf'),
(16, 12, 'Manager Script', 'Manage all things', '2022-10-31', '2022-10-31', 'New', 'sistem.docx');

-- --------------------------------------------------------

--
-- Table structure for table `task_categories`
--

CREATE TABLE `task_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `task_categories`
--

INSERT INTO `task_categories` (`id`, `name`) VALUES
(1, 'Audinabillah'),
(2, 'Megan'),
(3, 'Paleo'),
(4, 'Pither'),
(5, 'Wajanya'),
(6, 'Floran'),
(7, 'Ericatu'),
(8, 'Liang Bua'),
(9, 'Kertensis'),
(11, 'Austrolopither'),
(12, 'Qania'),
(13, 'Leonor'),
(14, 'Infanta'),
(15, 'Sofia');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_category_id` (`category_id`);

--
-- Indexes for table `task_categories`
--
ALTER TABLE `task_categories`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `task_categories`
--
ALTER TABLE `task_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `fk_category_id` FOREIGN KEY (`category_id`) REFERENCES `task_categories` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
