<?php
class Task_model extends CI_Model
{
    public $category_id;
    public $title;
    public $description;
    public $start_date;
    public $finish_date;
    public $status;
    public $doc_url;

    public function simpan($data)
    {
        $this->db->insert('tasks', $data);

        if (!in_array($data['status'], ['New', 'On Progress', 'Finish'])) {
            return false;
        }

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    public function ubah($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('tasks', $data);
        return $this->db->affected_rows() ? true : false;
    }

    public function hapus($id)
    {
        $this->db->delete('tasks', ['id' => $id]);
        //DELETE FROM task WHERE id = 10

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
        // bisa juga gini: return $this->db->affected_rows() ? true : false;
    }

    public function detail($id)
    {
        $this->db->select('*');
        $this->db->from('tasks');
        $this->db->where('id', $id);
        //SELECT * FROM 'task' WHERE 'id' = '10'

        $get = $this->db->get();

        if ($get->num_rows() != 0) {
            return $get->row_object();
        } else {
            return [];
        }
    }

    public function list_data($page, $per_page)
    {
        $this->db->select('*');
        $this->db->from('tasks');

        //begin pagination
        $offset = 0;
        if ($page >= 1) {
            $offset = $per_page * ($page - 1);
        }
        $this->db->limit($per_page, $offset);
        //end pagination

        $get = $this->db->get();

        if ($get->num_rows() > 0) {
            return $get->result();
        } else {
            return [];
        }
    }

    public function listda()
    {
        $this->db->select('*');
        $this->db->from('tasks');
        $get = $this->db->get();
        if ($get->num_rows() > 0) {
            return $get->result();
        } else {
            return [];
        }
    }
}
