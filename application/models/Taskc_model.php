<?php
class Taskc_model extends CI_Model
{
    public $name;

    public function simpan($data)
    {
        $this->db->insert('task_categories', $data);

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    public function ubah($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('task_categories', $data);
        return $this->db->affected_rows() ? true : false;
    }

    public function hapus($id)
    {
        $this->db->delete('task_categories', ['id' => $id]);
        //DELETE FROM task WHERE id = 10

        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
        // bisa juga gini: return $this->db->affected_rows() ? true : false;
    }

    public function detail($id)
    {
        $this->db->select('*');
        $this->db->from('task_categories');
        $this->db->where('id', $id);
        //SELECT * FROM 'task' WHERE 'id' = '10'

        $get = $this->db->get();

        if ($get->num_rows() != 0) {
            return $get->row_object();
        } else {
            return [];
        }
    }

    public function list_data($page, $per_page)
    {
        $this->db->select('*');
        $this->db->from('task_categories');

        //begin pagination
        $offset = 0;
        if ($page >= 1) {
            $offset = $per_page * ($page - 1);
        }
        $this->db->limit($per_page, $offset);
        //end pagination

        $get = $this->db->get();

        if ($get->num_rows() > 0) {
            return $get->result();
        } else {
            return [];
        }
    }

    public function listda()
    {
        $this->db->select('*');
        $this->db->from('task_categories');
        $get = $this->db->get();
        if ($get->num_rows() > 0) {
            return $get->result();
        } else {
            return [];
        }
    }
}
