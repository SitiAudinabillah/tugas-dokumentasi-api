<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Task_categories extends RestController
{
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('taskc_model');
    }
    public function index_post()
    {
        $data = $this->post();
        $success = $this->taskc_model->simpan($data);

        if ($success) {
            //condition when simpan() == true
            $this->response([
                'status' => $success,
                'message' => "Task successful upload",
                'data' => $data
            ], 200);
        } else {
            //condition when simpan() == false
            $this->response([
                'status' => $success,
                'message' => "Task unsuccessful upload"
            ], 400);
        }
    }

    public function index_delete()
    {
        $id = $this->get('id');

        if ($id) {
            $success = $this->taskc_model->hapus($id);

            if ($success) {
                $this->response([
                    'status' => true,
                    'message' => 'Task where ID = ' . $id . ' has been removed'
                ], 200);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'Task failed to remove'
                ], 400);
            }
        } else {
            $this->response([
                'status' => false,
                'message' => 'Undefined ID'
            ], 400);
        }
    }

    public function update_post()
    {
        $id = $this->get('id');
        $data = $this->post();

        $success = $this->taskc_model->ubah($id, $data);
        $newData = $this->taskc_model->detail($id);
        $this->response([
            'status' => true,
            'message' => "Task successful update",
            'data' => $newData
        ], 200);
    }

    public function index_get()
    {
        $id = $this->get('id');

        if ($id) {
            $success = $this->taskc_model->detail($id);

            if ($success) {
                $this->response([
                    'status' => true,
                    'message' => 'Task where ID = ' . $id . ' successful displayed',
                    'data' => $success
                ], 200);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'Task failed to display'
                ], 400);
            }
        } else {
            $this->response([
                'status' => false,
                'message' => 'Undefined ID'
            ], 400);
        }
    }

    public function list_get()
    {
        $page = $this->get('page') ?: 0;
        $per_page = $this->get('per-page') ?: 0;

        $data = $this->taskc_model->list_data($page, $per_page);

        $this->response([
            'status' => true,
            'message' => 'Data successful displayed',
            'data' => $data,
            'meta' => ['page' => $page, 'per-page' => $per_page]
        ], 200);
    }

    public function listda_get() //fungsi untuk menampilkan seluruh data
    {
        $data = $this->taskc_model->listda();
        $this->response([
            'status' => true,
            'message' => 'Data successful displayed',
            'data' => $data
        ], 200);
    }
}
